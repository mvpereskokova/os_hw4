#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/vmalloc.h>
#include <linux/string.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Marina Pereskokova");
MODULE_DESCRIPTION("Membuf - memory buffer");
MODULE_VERSION("0.1");

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX_DEV 256

dev_t dev = 0;
static struct cdev membuf_cdev[MAX_DEV];
static struct class *membuf_class;

static char* default_size = "1024;4;2;0;";
static char* size;
static int count = 4;
static char* memory[MAX_DEV];
static int length[MAX_DEV], open[MAX_DEV], new_lengths[MAX_DEV];
struct rw_semaphore rwsem[MAX_DEV], size_sem, count_sem, open_sem;

static ssize_t membuf_read(struct file *file, char __user *buf, size_t len, loff_t *off);
static ssize_t membuf_write(struct file *file, const char __user *buf, size_t len, loff_t *off);
static int membuf_open(struct inode *inode, struct file *file);
static int membuf_release(struct inode *, struct file *);

static struct file_operations membuf_ops =
{
        .owner      = THIS_MODULE,
        .read       = membuf_read,
        .write      = membuf_write,
        .open       = membuf_open,
	.release    = membuf_release,
};


static int init_device(int i, bool free_prev) {
        dev_t tmp_device;
        struct device* tmp_dev_res;
        int res;

        down_write(&rwsem[i]);
        cdev_init (&membuf_cdev[i], &membuf_ops);
        tmp_device = MKDEV(MAJOR(dev), i);
        res = cdev_add (&membuf_cdev[i], tmp_device, 1);
        up_write(&rwsem[i]);

        if (res < 0)
        {
                pr_err("membuf: device registering error\n");
                if (free_prev) {
                       for (int j = 0; j < count; j++) {
                                down_write(&rwsem[j]);
                                vfree(memory[j]);
                                up_write(&rwsem[j]);
                        }

                        for (int j = 0; j < i; j++) {
                                down_write(&rwsem[j]);
                                tmp_device=MKDEV(MAJOR(dev), j);
                                cdev_del(&membuf_cdev[j]);
                                device_destroy(membuf_class, tmp_device);
                                up_write(&rwsem[j]);
                        }
                        class_destroy (membuf_class);
                        unregister_chrdev_region (dev, MAX_DEV);
                }
                return res;
        }

        down_write(&rwsem[i]);
        tmp_dev_res = device_create(membuf_class, NULL, tmp_device, NULL, "membuf_%d", i);
        up_write(&rwsem[i]);

        if (IS_ERR(tmp_dev_res))
        {
                pr_err("membuf: error creating device\n");

                if (free_prev) {
                        for (int j = 0; j < count; j++) {
                                down_write(&rwsem[j]);
                                vfree(memory[j]);
                                up_write(&rwsem[j]);
                        }

                        for (int j = 0; j < i; j++) {
                                down_write(&rwsem[j]);
                                tmp_device=MKDEV(MAJOR(dev), j);
                                cdev_del(&membuf_cdev[j]);
                                device_destroy(membuf_class, tmp_device);
                                up_write(&rwsem[j]);
                        }
                }

                down_write(&rwsem[i]);
                cdev_del(&membuf_cdev[i]);
                up_write(&rwsem[i]);

                if (free_prev) {
                        class_destroy (membuf_class);
                        unregister_chrdev_region (dev, MAX_DEV);
                }

                return -1;
        }

        pr_info("device %d inited", i);
        return 0;
}

int param_count_set(const char *val, const struct kernel_param *kp)
{
        dev_t tmp_device;
        int old_count = count, res, bound = 0, num = 0;
        char* new_size;

        down_write(&count_sem);
        res = param_set_int(val, kp);
        up_write(&count_sem);

	down_read(&open_sem);

        if (res == 0)
        {
                pr_info("membuf: new count = %d\n", count);

                if (old_count < count) {
			down_write(&size_sem);

                        bound = strlen(size) + 2 * (count - old_count);
                        new_size = vmalloc(bound + 1);
                        memset(new_size, 0, bound  + 1);
                        memcpy(new_size, size, strlen(size));
                        for (int i = strlen(size); i < bound; i += 2) {
                                new_size[i] = '0';
                                new_size[i + 1] = ';';
                        }
                        vfree(size);
                        size = new_size;

                        pr_info("new_size %s", size);
                        up_write(&size_sem);

                        for (int i = old_count; i < count; i++) {
                                down_write(&rwsem[i]);

                                length[i] = 0;
                                memory[i] = 0;

                                up_write(&rwsem[i]);

                                if ((res = init_device(i, false)) < 0) {
					up_read(&open_sem);
                                        return res;
			     	}
                        }
                }

                if (old_count > count) {
			for (int i = count; i < old_count; i++) {
				if (open[i] > 0) {
					count = old_count;
					pr_err("Can't del open membuf_%d", i);
					up_read(&open_sem);
					return -1;
				}
			}


                        down_write(&size_sem);

                        for (int i = 0; i < strlen(size); i++) {
                                if (size[i] == ';') {
                                        num += 1;
                                        if (num == count) {
                                                bound = i + 1;
                                                break;
                                        }
                                }
                        }

                        new_size = vmalloc(bound + 1);
                        memset(new_size, 0, bound + 1);
                        memcpy(new_size, size, bound);
                        vfree(size);
                        size = new_size;

                        up_write(&size_sem);
                        pr_info("new_size %s", size);

                        for (int i = count; i < old_count; i++) {
                                down_write(&rwsem[i]);

                                tmp_device=MKDEV(MAJOR(dev), i);
                                cdev_del(&membuf_cdev[i]);
                                device_destroy(membuf_class, tmp_device);
                                vfree(memory[i]);

                                up_write(&rwsem[i]);
                        }
                }
		up_read(&open_sem);
                return 0;
        }

	up_read(&open_sem);
        return res;
}

int param_count_get(char *buffer, const struct kernel_param *kp) {
        int res;

        down_read(&count_sem);
        res = param_get_int(buffer, kp);
        up_read(&count_sem);

        return res;
}


const struct kernel_param_ops param_count_ops =
{
        .set = &param_count_set,
        .get = &param_count_get,
};

module_param_cb(count, &param_count_ops, &count, 0644);
MODULE_PARM_DESC(count, "Membuf file count");

int parse_params(const char* size, int* lengths) {
        int x, i = 0;
        int offset = 0;

        while (sscanf(size, "%d;%n", &x, &offset) > 0) {
                if (i == count) {
                        pr_info("membuf: too much size params");
                        return -1;
                }
                pr_info("File %d has size %d", i, lengths[i]);

                lengths[i] = x;
                size += offset;
                i += 1;
        }

        if (i != count) {
                pr_info("count %d need to be %d", i, count);
                return -1;
        }
        return 0;
}

int param_size_set(const char *val, const struct kernel_param *kp)
{
        char* new_memory[MAX_DEV];
        int parse_res;

        pr_info("param_size_set");

	down_read(&open_sem);
	for (int i = 0; i < count; i++) {
		if (open[i] > 0) {
			pr_err("Can't change size while files are open");
			up_read(&open_sem);
			return -1;
		}
	}

        parse_res = parse_params(val, new_lengths);
        if (parse_res < 0) {
                pr_err("parse size problems");
                up_read(&open_sem);
		return -1;
        }

        pr_info("membuf: new sizes = %s\n", val);


        for (int i = 0; i < count; i++) {
                if (new_lengths[i] == 0) {
                        new_memory[i] = NULL;
                        continue;
                }

                new_memory[i] = vmalloc(new_lengths[i]);
                if (new_memory[i] == NULL)
                {
                        for (int j = 0; j < i; j++) {
                                vfree(new_memory[j]);
                        }
                        pr_err("problems with malloc");
			up_read(&open_sem);
                        return -1;
                }
        }

        pr_info("new memory inited");

        for (int i = 0; i < count; i++) {
                down_write(&rwsem[i]);
                if (new_lengths[i] > 0) {
                        memset(new_memory[i], 0, new_lengths[i]);
                        memcpy(new_memory[i], memory[i], MIN(length[i], new_lengths[i]));
                }

                vfree(memory[i]);

                memory[i] = new_memory[i];
                length[i] = new_lengths[i];
                pr_info("size set for file %d", i);

                up_write(&rwsem[i]);

        }

        down_write(&size_sem);

        vfree(size);
        size = vmalloc(strlen(val) + 1);
        memset(size, 0, strlen(val) + 1);
        memcpy(size, val, strlen(val));

        up_write(&size_sem);
	up_read(&open_sem);

        return 0;
}

int param_size_get(char* buffer, const struct kernel_param* kp) {
        int res;

        down_read(&size_sem);
        res = scnprintf(buffer, PAGE_SIZE, "%s\n", size);
        up_read(&size_sem);

        return res;
}


const struct kernel_param_ops param_size_ops =
{
        .set = &param_size_set,
        .get = &param_size_get,
};

module_param_cb(size, &param_size_ops, &size, 0644);
MODULE_PARM_DESC(size, "Membuf memory size");

static ssize_t membuf_read(struct file *file, char __user *buf, size_t len, loff_t *off)
{
        unsigned long to_copy, not_copied, copied;
        int i = (uintptr_t)file->private_data;

        down_read(&rwsem[i]);

        pr_info("membuf read, offset %lld, size %lu\n", *off, len);

        if (*off >= length[i])
        {
                up_read(&rwsem[i]);
                return 0;
        }

        to_copy = len;
        if (*off + len > length[i])
        {
                to_copy = length[i] - *off;
        }

        not_copied = copy_to_user(buf, memory[i] + *off, to_copy);
        copied = to_copy - not_copied;
        *off += copied;

        up_read(&rwsem[i]);

        return copied;
}

static ssize_t membuf_write(struct file *file, const char __user *buf, size_t len, loff_t *off)
{
        unsigned long not_wrote, wrote;
        char* tmp_buf;
        int i = (uintptr_t)file->private_data;

        down_write(&rwsem[i]);

        pr_info("membuf write, offset: %lld, size: %lu\n", *off, len);

	if (*off >= length[i])
	{
		up_write(&rwsem[i]);
		return -ENOSPC;
	}

        if (*off + len > length[i])
        {
		len = length[i] - (*off);
        }

        tmp_buf = vmalloc(len);
        if (tmp_buf == NULL)
        {
                up_write(&rwsem[i]);
                return -ENOMEM;
        }

        not_wrote = copy_from_user(tmp_buf, buf, len);
        wrote = len - not_wrote;
        memcpy(memory[i] + *off, tmp_buf, wrote);
	*off += wrote;

        vfree(tmp_buf);

        up_write(&rwsem[i]);

        return wrote;
}

static int membuf_open(struct inode *inode, struct file *file) {
        uintptr_t minor;
	
	down_write(&open_sem);
	
	minor = MINOR(inode->i_rdev);
        file->private_data = (void *) minor;
        open[minor]++;

	up_write(&open_sem);
	return 0;
}


static int membuf_release(struct inode *inode, struct file *file) {
	uintptr_t minor;
	
	down_write(&open_sem);

	minor = MINOR(inode->i_rdev);
        open[minor]--;

	up_write(&open_sem);
	return 0;
}


static int __init membuf_start(void)
{
        int res;

        if ((res = alloc_chrdev_region(&dev, 0, MAX_DEV, "membuf")) < 0)
        {
                pr_err("Error allocating major number\n");
                return res;
        }
        pr_info("membuf load: Major = %d Minor = %d\n", MAJOR(dev), MINOR(dev));

        size = vmalloc(strlen(default_size));
        if (size == NULL) {
                pr_err("problems with malloc");
                unregister_chrdev_region(dev, MAX_DEV);
                return -1;
        }
        memset(size, 0, strlen(default_size)+1);
        memcpy(size, default_size, strlen(default_size));

        res = parse_params(size, new_lengths);
        if (res < 0) {
                pr_err("parse size error");
                unregister_chrdev_region(dev, MAX_DEV);
                return -1;
        }

        pr_info("membuf: load size=%s", size);

        for (int i = 0; i < MAX_DEV; i++) {
                init_rwsem(&rwsem[i]);
        }
        init_rwsem(&count_sem);
        init_rwsem(&size_sem);
	init_rwsem(&open_sem);

        for (int i = 0; i < count; i++) {
                length[i] = new_lengths[i];
                if (length[i] == 0) {
                        memory[i] = NULL;
                        continue;
                }
                memory[i] = vmalloc(length[i]);
                if (memory[i] == NULL)
                {
                        pr_err("Problems with vmalloc\n");
                        for (int j = 0; j < i; j++) {
                                vfree(memory[j]);
                        }
                        unregister_chrdev_region(dev, MAX_DEV);
                        return -1;
                }
                memset(memory[i], 0, length[i]);
        }

        if (IS_ERR(membuf_class = class_create(THIS_MODULE, "membuf_class")))
        {
                unregister_chrdev_region (dev, MAX_DEV);

                for (int j = 0; j < count; j++) {
                        down_write(&rwsem[j]);
                        vfree(memory[j]);
                        up_write(&rwsem[j]);
                }

                return -1;
        }

        for (int i = 0; i < count; i++) {
                if ((res = init_device(i, true)) < 0) {
                        return res;
                }
        }

        return 0;
}

static void __exit membuf_end(void)
{
        dev_t tmp_device;

        for (int j = 0; j < count; j++) {
                down_write(&rwsem[j]);

                tmp_device=MKDEV(MAJOR(dev), j);
                cdev_del(&membuf_cdev[j]);
                device_destroy(membuf_class, tmp_device);
                vfree(memory[j]);

                up_write(&rwsem[j]);
        }
        class_destroy (membuf_class);
        unregister_chrdev_region (dev, MAX_DEV);

        pr_info("membuf: unload\n");
}

module_init(membuf_start);
module_exit(membuf_end);

